#include <iostream>

using namespace std;

int n, m = 1, a[16];
int sol[16], viz[16];

void citire(){
    int x;
    cin >> n;
    for(int i = 0; i < n; i++){
        cin >> x;
        if(x % 2 == 1)
            a[m++] = x;
    }
}

void afisare(int l){
    for(int i = 1; i <= l; i++){
        cout << a[sol[i]] << " ";
    }
    cout << '\n';
}

void backtr(int k, int l){
    if(k == l + 1){
        afisare(l);
        return;
    }
    for(int v = sol[k-1] + 1; v < m; v++){
        if(viz[v] == 0){
            sol[k] = v;
            backtr(k+1, l);
        }
    }
}

int main(int argc, char const *argv[])
{
    citire();
    for(int i = 1; i <= m; i++){
        backtr(1, i);
    }
    return 0;
}
